package main

import (
  "flag"
  "fmt"
  "os"
  "time"

  "k8s.io/client-go/informers"
  "k8s.io/client-go/kubernetes"
  "k8s.io/client-go/rest"
  "k8s.io/client-go/tools/clientcmd"
)

func main() {

  // I get the kubeconfig either from default value or from kubernetes flag
  kubeconfig := flag.String("kubeconfig", os.Getenv("HOME")+"/.kube/config", "kubeconfig location")

  flag.Parse()

  config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
  if err != nil {
    // hancle err
    fmt.Printf("error %s building config from flags\n", err.Error())
    // if this fails, I have to check whether the client is running from inside a cluster..
    config, err = rest.InClusterConfig()
    if err != nil {
      fmt.Printf("error %s getting incluster config\n", err.Error())
    }
  }

  clientset, err := kubernetes.NewForConfig(config)

  if err != nil {
    panic(err)
  }

  // informers

  ch := make(chan struct{})
  informerFactory := informers.NewSharedInformerFactory(clientset, 10*time.Minute)
  c := newController(clientset, informerFactory.Apps().V1().Deployments())
  informerFactory.Start(ch)

  c.run(ch)

}
