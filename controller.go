package main

import (
  "context"
  "fmt"
  "time"

  corev1 "k8s.io/api/core/v1"
  netv1 "k8s.io/api/networking/v1"
  kerr "k8s.io/apimachinery/pkg/api/errors"
  metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
  "k8s.io/apimachinery/pkg/util/wait"
  appsinformers "k8s.io/client-go/informers/apps/v1"
  "k8s.io/client-go/kubernetes"
  applisters "k8s.io/client-go/listers/apps/v1"
  "k8s.io/client-go/tools/cache"
  "k8s.io/client-go/util/workqueue"
)

type controller struct {
  // we need a clientset in order to create (manage) objects... to interact with k8s cluster
  clientset kubernetes.Interface
  // we need a lister in order to access the informer s cache
  depLister applisters.DeploymentLister
  // we should have a way to figure out if the cache has been synced or updated
  depCacheSynced cache.InformerSynced
  // we need a queue, as soon as the objects are coming from k8s, these need to be put in a queue to be processed
  queue workqueue.RateLimitingInterface
}

func newController(cs kubernetes.Interface, depInformer appsinformers.DeploymentInformer) *controller {
  c := &controller{
    clientset:      cs,
    depLister:      depInformer.Lister(),
    depCacheSynced: depInformer.Informer().HasSynced,
    queue:          workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "ekspose"),
  }

  depInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
    AddFunc:    c.handleAdd,
    DeleteFunc: c.handleDelete,
  })

  return c

}

func (c *controller) run(ch <-chan struct{}) {

  fmt.Println("Starting controller...")
  // wait for cache to be synched
  if !cache.WaitForCacheSync(ch, c.depCacheSynced) {
    // if not done, there was something wrong when trying to sync the cache...
    fmt.Printf("waiting for cache to be synced\n")
  }

  // ho bisogno di chiamare il metodo worker in modo periodico...
  // wait.Until ... chiamera' una func ogni tot period finche un canale non sara' chiuso
  // se non chiudiamo il canale, questa func viene chiamata ...
  go wait.Until(c.worker, 1*time.Second, ch) // devo farla diverntare una go routine perche' voglio processare piu' workers in parallelo

  <-ch

}

func (c *controller) worker() {
  for c.processItem() {

  }
}

func (c *controller) processItem() bool {
  item, shutdown := c.queue.Get()
  if shutdown {
    return false
  }

  // al termine della processazione, per evitare che l'oggetto venga processato di nuovo
  defer c.queue.Forget(item)

  // questo metodo serve per ottenere il nomde dell'oggetto a partire dall obj letto dalla queue
  key, err := cache.MetaNamespaceKeyFunc(item)
  if err != nil {
    fmt.Printf("getting key from cache %s\n", err.Error())
  }
  //fmt.Println(key)

  // con questo metodo posso splittare la key in ns e name
  namespace, name, err := cache.SplitMetaNamespaceKey(key)
  if err != nil {
    fmt.Printf("splitting key into ns and name %s\n", err.Error())
    return false
  }

  // check if the object has been deleted in k8s cluster
  ctx := context.Background()
  _, err = c.clientset.AppsV1().Deployments(namespace).Get(ctx, name, metav1.GetOptions{})
  if err != nil {
    if kerr.IsNotFound(err) {
      fmt.Printf("handle delete for deployment %s\n", name)

      err = c.clientset.CoreV1().Services(namespace).Delete(ctx, name, metav1.DeleteOptions{})
      if err != nil {
        fmt.Printf("deleting service %s, error %s\n", name, err.Error())
        return false
      }

      err = c.clientset.NetworkingV1().Ingresses(namespace).Delete(ctx, name, metav1.DeleteOptions{})
      if err != nil {
        fmt.Printf("deleting ingress %s, error %s\n", name, err.Error())
        return false
      }

      return true
    }
  }

  if namespace != "default" {
    return false
  }

  fmt.Println(namespace, name)

  //NB. noi sappiamo che questo oggetto e' un deployment, perche' l'informer che abbiamo creato e' un deployment informer
  // ora devo creare un service...

  err = c.syncDeployment(namespace, name)

  if err != nil {
    // retry...
    fmt.Printf("syncing deployment error %s\n", err.Error())
    return false
  }

  return true
}

func (c *controller) syncDeployment(ns, name string) error {
  ctx := context.Background()

  // let-s get the deployment
  // la cosa figa e' che lo otteniamo dal lister e non dall'api... no need to call the api directly
  dep, err := c.depLister.Deployments(ns).Get(name)
  if err != nil {
    fmt.Printf("getting deployment from lister %s\n", err.Error())

  }

  // Create a service
  // we have to modify this to figure out the pod our deployment's container is listening on
  svc := corev1.Service{
    ObjectMeta: metav1.ObjectMeta{
      Name:      name,
      Namespace: ns,
    },
    Spec: corev1.ServiceSpec{
      Ports: []corev1.ServicePort{
        {
          Name: "http",
          Port: 80,
        },
      },
      Selector: dep.Spec.Template.Labels,
    },
  }

  s, err := c.clientset.CoreV1().Services(ns).Create(ctx, &svc, metav1.CreateOptions{})

  if err != nil {
    fmt.Printf("creating service %s\n", err.Error())
    return err
  }

  // Create an ingress
  return createIngress(ctx, c.clientset, s)
}

func (c *controller) handleAdd(obj interface{}) {
  // I add the object to the queue in order to be processed
  c.queue.Add(obj)
  fmt.Println("Called handle Add")

  // u, ok := obj.(*v1.Deployment)
  // if !ok {
  //   fmt.Println("not a deployment")
  // }

  // fmt.Printf("Handle Add called on Deployment: %s\n", u.GetName())
}

func (c *controller) handleDelete(obj interface{}) {
  c.queue.Add(obj)
  fmt.Println("Called handle Delete")

  // u, ok := obj.(*v1.Deployment)
  // if !ok {
  //   fmt.Println("not a deployment")
  // }

  // fmt.Printf("Deleted Deployment: %s\n", u.GetName())

}

func createIngress(ctx context.Context, client kubernetes.Interface, svc *corev1.Service) error {
  pathType := "Prefix"
  ingressClass := "nginx"

  ingress := &netv1.Ingress{
    ObjectMeta: metav1.ObjectMeta{
      Name:      svc.Name,
      Namespace: svc.Namespace,
      Annotations: map[string]string{
        "nginx.ingress.kubernetes.io/rewrite-target": "/",
      },
    },
    Spec: netv1.IngressSpec{
      IngressClassName: &ingressClass,
      Rules: []netv1.IngressRule{
        {
          Host: "marlem.com",
          IngressRuleValue: netv1.IngressRuleValue{
            HTTP: &netv1.HTTPIngressRuleValue{
              Paths: []netv1.HTTPIngressPath{
                {
                  Path:     fmt.Sprintf("/%s", svc.Name),
                  PathType: (*netv1.PathType)(&pathType),
                  Backend: netv1.IngressBackend{
                    Service: &netv1.IngressServiceBackend{
                      Name: svc.Name,
                      Port: netv1.ServiceBackendPort{
                        Number: 80,
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  }

  _, err := client.NetworkingV1().Ingresses(svc.Namespace).Create(ctx, ingress, metav1.CreateOptions{})

  return err
}
