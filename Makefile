M="."
VERSION=0.1.1
.PHONY: git
git:
	git add . && git commit -m "$(M)" && git push

.PHONY: build
build:
	GOOS=linux GOARCH=amd64 go build -o ekspose

.PHONY: run
run:
	@go run *.go

.PHONY: build-image
build-image:
	nerdctl build -t xmarlem/ekspose:$(VERSION) .

.PHONY: push-image
push-image:
	nerdctl push xmarlem/ekspose:$(VERSION)
