# simple-k8s-goclient


This is a simple controller I am writing from scratch.

GOAL: I want to listen to Deployments and expose them to connect with Service and Ingress.
i.e. when detecting a deployment is created, this controller will create a service and an ingress for that.

NOTES:

These are the basic imports, minimum to do:

```go
  metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
  "k8s.io/client-go/kubernetes"
  "k8s.io/client-go/tools/clientcmd"
```

First you need the flag to get the kubeconfig path....
Then use the clientcmd to BuildConfig from flag.
Then kubernetes to get from NewForConfig(config)... the clientset.

The clientset is what we need in order to get access to a set of clients, each one, specialized to CRUD each kind of resource.






## Getting started

start rancher
update version in makefile
make build build-image push-image
deploy to cluster with k apply -f manifests


TODO

I created a new controller struct with constructor.
For that we need a few objects.

```go
  // we need a clientset in order to create (manage) objects... to interact with k8s cluster
  clientset kubernetes.Interface
  // we need a lister in order to access the informer s cache
  depLister applisters.DeploymentLister
  // we should have a way to figure out if the cache has been synced or updated
  depCacheSynced cache.InformerSynced
  // we need a queue, as soon as the objects are coming from k8s, these need to be put in a queue to be processed
  queue workqueue.RateLimitingInterface
```

Ho bisogno poi di un informer...
Nel nostro caso vogliamo controllare i Deployment objects, quindi prendero' un deploy informer.


Ho poi creato un metodo run per far partire il controller.
Nel run passo un canale dall'esterno, che se viene chiuso, il controller esce.

La prima cosa da fare quando si fa partire il controller e' controllare che la cache sia sincronizzata.
Infine, ho bisogno di chiamare un metodo "worker" dove mettero' tutta la logica di processazione, periodicamente finche il canale ch viene chiuso...faccio questo con il metodo cache.Until.


Il metodo worker, non fa altro che attendere in un ciclo infinito per un altro metodo: processItem.

processItem restituisce un bool... se dovesse restituire false, il for terminerebbe.


Nel costruttore del controller abbiamo registrato gli handler.
La logica e' che se vengo notificato, devo mettere l'obj in una queue affinche' possa essere processato.

Quindi, negli handler metto solo la Add ...
Nella processItem, la prima cosa che faccio e' estrarre un oggetto dalla queue..

Una volta recuperato nome e cognome del deployment chiamo un metodo da me scritto: syncDeployment
In questo metodo scriviamo quello che vogliamo fare... cioe' creare service e ingress.





## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/xmarlem-k8s/simple-k8s-goclient.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/xmarlem-k8s/simple-k8s-goclient/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
